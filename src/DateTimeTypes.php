<?php

namespace GqlDatetime;

use GqlDatetime\Scalars\DateScalarType;
use GqlDatetime\Scalars\DateTimeScalarType;
use GqlDatetime\Scalars\TimeScalarType;
use GraphQL\Type\Definition\ScalarType;

/**
 * Фасад для генерации типов даты и времени
 */
class DateTimeTypes
{
    /**
     * Генерация скалярного типа даты
     *
     * @param string|null $format
     * @return ScalarType
     */
    public static function date(string $format = null): ScalarType {
        return new DateScalarType($format);
    }

    /**
     * Генерация скалярного типа времени
     *
     * @param string|null $format
     * @return ScalarType
     */
    public static function time(string $format = null): ScalarType {
        return new TimeScalarType($format);
    }

    /**
     * Генерация скалярного типа даты и времени
     *
     * @param string|null $format
     * @return ScalarType
     */
    public static function dateTime(string $format = null): ScalarType {
        return new DateTimeScalarType($format);
    }
}
<?php

namespace GqlDatetime\Scalars;

use DateTimeImmutable;
use DateTimeInterface;
use Exception;
use GraphQL\Error\Error;
use GraphQL\Error\InvariantViolation;
use GraphQL\Language\AST\BooleanValueNode;
use GraphQL\Language\AST\FloatValueNode;
use GraphQL\Language\AST\IntValueNode;
use GraphQL\Language\AST\Node;
use GraphQL\Language\AST\NullValueNode;
use GraphQL\Language\AST\StringValueNode;
use GraphQL\Type\Definition\ScalarType;
use GraphQL\Utils\Utils;

/**
 * Абстрактный класс для скаляного типа даты и времени
 */
abstract class AbstractDateTimeScalar extends ScalarType
{
    /** @var string */
    protected $format;

    /**
     * AbstractDateTimeScalar constructor.
     *
     * @param string|null $format
     */
    public function __construct(string $format = null)
    {
        parent::__construct([]);
        if (null === $format) {
            $format = $this->getDefaultFormat();
        }

        /** @var string format */
        $this->format = $format;
    }

    /**
     * Получение формата по умолчанию для текущего типа
     *
     * @return string
     */
    abstract protected function getDefaultFormat(): string;

    /**
     * Serializes an internal value to include in a response.
     *
     * @param mixed $value
     *
     * @return mixed
     *
     * @throws Error
     */
    public function serialize($value)
    {
        if (is_string($value)) {
            try {
                $newValue = DateTimeImmutable::createFromFormat($this->format, $value);

                if ($newValue instanceof DateTimeInterface) {
                    $value = $newValue;
                }
            } catch (Exception $exception) {
            }
        }

        if (is_string($value)) {
            try {
                $newValue = new DateTimeImmutable($value);

                if ($newValue instanceof DateTimeInterface) {
                    $value = $newValue;
                }
            } catch (Exception $exception) {
            }
        }

        if (!$value instanceof DateTimeInterface) {
            throw new InvariantViolation('DateTime is not an instance of DateTimeInterface: ' . Utils::printSafe($value));
        }

        return $value->format($this->format);
    }

    /**
     * Parses an externally provided value (query variable) to use as an input
     *
     * In the case of an invalid value this method must throw an Exception
     *
     * @param mixed $value
     *
     * @return mixed
     *
     * @throws Error
     */
    public function parseValue($value)
    {
        return DateTimeImmutable::createFromFormat($this->format, $value) ?: null;
    }

    /**
     * Parses an externally provided literal value (hardcoded in GraphQL query) to use as an input
     *
     * In the case of an invalid node or value this method must throw an Exception
     *
     * @param IntValueNode|FloatValueNode|StringValueNode|BooleanValueNode|NullValueNode $valueNode
     * @param mixed[]|null $variables
     *
     * @return mixed
     *
     * @throws Exception
     */
    public function parseLiteral(Node $valueNode, ?array $variables = null)
    {
        if ($valueNode instanceof StringValueNode) {
            return $valueNode->value;
        }

        return null;
    }
}
<?php

namespace GqlDatetime\Scalars;

/**
 * Скалярный тип даты и времени
 */
class DateScalarType extends AbstractDateTimeScalar
{
    /**
     * Получение формата по умолчанию для текущего типа
     *
     * @return string
     */
    protected function getDefaultFormat(): string
    {
        return "Y-m-d";
    }
}
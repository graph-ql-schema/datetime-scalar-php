<?php

namespace GqlDatetime\Scalars;

/**
 * Скалярный тип времени
 */
class TimeScalarType extends AbstractDateTimeScalar
{
    /**
     * Получение формата по умолчанию для текущего типа
     *
     * @return string
     */
    protected function getDefaultFormat(): string
    {
        return "H:i:s";
    }
}
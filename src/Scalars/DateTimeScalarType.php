<?php

namespace GqlDatetime\Scalars;

/**
 * Скалярный тип даты и времени
 */
class DateTimeScalarType extends AbstractDateTimeScalar
{
    /**
     * Получение формата по умолчанию для текущего типа
     *
     * @return string
     */
    protected function getDefaultFormat(): string
    {
        return DATE_RFC3339;
    }
}
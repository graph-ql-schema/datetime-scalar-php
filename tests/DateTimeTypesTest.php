<?php

namespace GqlDatetime\Tests;

use DateTime;
use GqlDatetime\DateTimeTypes;
use GqlDatetime\Scalars\DateScalarType;
use GqlDatetime\Scalars\DateTimeScalarType;
use GqlDatetime\Scalars\TimeScalarType;
use GraphQL\Error\Error;
use PHPUnit\Framework\TestCase;

class DateTimeTypesTest extends TestCase
{
    public function testDate()
    {
        $this->assertEquals(new DateScalarType(), DateTimeTypes::date());
    }

    public function testTime()
    {
        $this->assertEquals(new TimeScalarType(), DateTimeTypes::time());
    }

    public function testDateTime()
    {
        $this->assertEquals(new DateTimeScalarType(), DateTimeTypes::dateTime());
    }

    public function dataForSerialize()
    {
        return [
            ["2020-01-01T11:00:00", "2020-01-01T11:00:00+00:00"],
            ["2020-01-01 11:00:00", "2020-01-01T11:00:00+00:00"],
            ["2020-01-01T11:00:00Z", "2020-01-01T11:00:00+00:00"],
            [new DateTime("2020-01-01T11:00:00"), "2020-01-01T11:00:00+00:00"],
        ];
    }

    /**
     * @dataProvider dataForSerialize
     * @param $value
     * @param $expected
     * @throws Error
     */
    public function testSerialize($value, $expected)
    {
        $dateTimeScalar = DateTimeTypes::dateTime();
        $dateTimeScalar->serialize($value);

        $this->assertEquals($expected, $dateTimeScalar->serialize("2020-01-01T11:00:00"));
    }
}